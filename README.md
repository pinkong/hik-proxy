# 海康视频接口调用代理

该项目为调用海康综合安防管理平台的视频的代理，方便前端调用视频相关接口。同时支持海康萤石云。

海康综合安防管理平台视频接入文档地址为：[iSecure Center](https://open.hikvision.com/docs/docId?productId=083bafe42ff34af7842aa3d6d8fa47f6)
萤石云接入指南: [萤石云文档](https://open.ys7.com/doc/zh/)

该项目代码库：[github](https://github.com/jmaxhu/hik-proxy) [码云](https://gitee.com/maxwell/hik-proxy)

## 系统截图

![截图](./screenshot.png)

## 系统运行

### 本地运行

```shell script
./mvnw spring-boot:run
```

### 本地打包

```shell script
./mvnw package
```

### docker 运行

```shell script
docker run -p "8090:8090" \
      -e HIK_KEYS[0]_IP=xxx \
      -e HIK_KEYS[0]_APPKEY=change \
      -e HIK_KEYS[0]_APPSECRET=change \
      jmaxhu/hik-proxy:latest
```

### 使用 docker compose

创建一个 docker-compose.yml 的文件，内容如下。

```
version: '3'

services:
  hik-proxy:
    image: jmaxhu/hik-proxy
    container_name: hik-proxy
    restart: always
    ports:
      - "8090:8090"
    environment:
      - HIK_KEYS[0]_IP=change it
      - HIK_KEYS[0]_APPKEY=change it
      - HIK_KEYS[0]_APPSECRET=change it
```

然后切换到包含该文件的目录，运行命令：

```
docker-compose up -d
```

### 生成 docker 镜像

创建本地镜像

```sh
./mvnw compile jib:dockerBuild
```

生成镜像并上传到 docker hub

```sh
./mvnw compile jib:build
```

## 使用说明

1. 综合安防管理平台接口地址为： **/api/hik**
2. 萤石云接口地址为： **/api/ys**

只接收：**POST** 请求

请求的参数为：

 - ip, 视频监控平台的ip地址(萤石云不需要)
 - apiUrl, 实际要调用的平台的api地址。
 - apiParams, 综合安防平台api接收的参数，以JSON格式序列化为字符串。萤石云api接收参数以 x-www-form-urlencoded 形式传输。
 - appKey，在各平台上注册应用的 appKey
 - appSecret(可选), 在各平台上注册应用的 appSecret, 为了安全可以把该参数放在配置文件中。
 
 返回结果为调用视频api地址的原始结果。
 
 ## 示例
 
### 综合安防管理平台

发送请求(请用真实ip代替):
 
 ```shell script
curl --location --request POST 'http://localhost:8090/api/hik' \
--header 'Content-Type: application/json' \
--data-raw '{
	"ip": "111.222.111.111",
	"apiUrl": "/api/resource/v1/camera/advance/cameraList",
	"apiParams": "{\"pageNo\":1,\"pageSize\":5}"
}'
```

返回结果：
```
HTTP/1.1 200 
Location: /api/hik
Content-Type: application/json
Content-Length: 4957
Date: Thu, 19 Mar 2020 14:26:50 GMT
Connection: close

{"code":"0","msg":"SUCCESS","data":{"total":6,"pageNo":1,"pageSize":5,"list":[{"altitude":null,"cameraIndexCode":"69c41a798a094b11a022b6ae5b22ddaf","cameraName":"第三通道","cameraType":0,"cameraTypeName":"枪"...
```

### 萤石云

发送请求(请用真实ip代替):

 ```shell script
curl --location --request POST 'http://localhost:8090/api/ys' \
--header 'Content-Type: application/json' \
--data-raw '{
	"appKey": "xxxxxxxxx",
	"apiUrl": "/api/lapp/v2/live/address/get",
	"apiParams": "deviceSerial=1212&protocol=hls"
}'
```

返回结果：
```
HTTP/1.1 200 
Location: /api/hik
Content-Type: application/json
Content-Length: 4957
Date: Thu, 19 Mar 2020 14:26:50 GMT
Connection: close

{"code":"200","msg":"Operation succeeded","data":{"id": "1212312434", "url": "xxxx"...
```

