package com.example.hik.config;

import com.dahuatech.icc.exception.ClientException;
import com.dahuatech.icc.oauth.http.DefaultClient;
import com.dahuatech.icc.oauth.http.IClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Spring Bean配置
 * @author KJ
 */
@Configuration
public class AppConfig {

    @Bean
    public IClient iccDefaultClient() throws ClientException {
        return new DefaultClient();
    }
}
