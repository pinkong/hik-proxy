package com.example.hik.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * @author maxwell
 */
@ConfigurationProperties(prefix = "hik")
public class HikProperties {

    private List<VideoKey> keys = new ArrayList<>();
    private List<YsConfig> ys = new ArrayList<>();

    /**
     * 海康监控平台配置项
     */
    public List<VideoKey> getKeys() {
        return keys;
    }

    /**
     * 萤石云配置项
     */
    public List<YsConfig> getYs() {
        return ys;
    }

    /**
     * 海康监控平台配置
     */
    public static class VideoKey {
        private String ip;
        private String appKey;
        private String appSecret;

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getAppSecret() {
            return appSecret;
        }

        public String getAppKey() {
            return appKey;
        }

        public void setAppSecret(String appSecret) {
            this.appSecret = appSecret;
        }

        public void setAppKey(String appKey) {
            this.appKey = appKey;
        }
    }

    /**
     * 萤石云的配置
     */
    public static class YsConfig {
        private String appKey;
        private String appSecret;

        public String getAppKey() {
            return appKey;
        }

        public void setAppKey(String appKey) {
            this.appKey = appKey;
        }

        public String getAppSecret() {
            return appSecret;
        }

        public void setAppSecret(String appSecret) {
            this.appSecret = appSecret;
        }
    }
}
