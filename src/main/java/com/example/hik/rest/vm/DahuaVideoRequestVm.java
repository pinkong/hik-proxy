package com.example.hik.rest.vm;

/**
 * @author KJ
 *
 * 大华取流请求
 */
public class DahuaVideoRequestVm {

    /**
     * 通道编码
     *
     * <p>例 1000150$7$0$0
     */
    private String channelId;

    /**
     * 码流类型。
     *
     * <p>1-主码流，2-辅码流
     */
    private String streamType;

    /**
     * 拉流方式
     *
     * 目前支持hls和rtsp
     */
    private String type = "hls";

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getStreamType() {
        return streamType;
    }

    public void setStreamType(String streamType) {
        this.streamType = streamType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "DahuaRequest{" +
                "channelId='" + channelId + '\'' +
                ", streamType='" + streamType + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
