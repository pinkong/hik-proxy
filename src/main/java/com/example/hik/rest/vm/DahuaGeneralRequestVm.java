package com.example.hik.rest.vm;

/**
 * @author KJ
 *
 * 大华通用请求参数
 */
public class DahuaGeneralRequestVm {

    /**
     * 请求url
     */
    private String apiUrl;

    /**
     * 请求体
     */
    private String apiParam;

    /**
     * 请求方式
     */
    private String apiMethod = "POST";

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getApiParam() {
        return apiParam;
    }

    public void setApiParam(String apiParam) {
        this.apiParam = apiParam;
    }

    public String getApiMethod() {
        return apiMethod;
    }

    public void setApiMethod(String apiMethod) {
        this.apiMethod = apiMethod;
    }

    @Override
    public String toString() {
        return "DahuaGeneralRequestVm{" +
                "apiUrl='" + apiUrl + '\'' +
                ", apiParam='" + apiParam + '\'' +
                ", apiMethod='" + apiMethod + '\'' +
                '}';
    }
}
