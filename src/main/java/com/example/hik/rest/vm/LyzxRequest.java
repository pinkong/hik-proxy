package com.example.hik.rest.vm;

/**
 * 流域中心取流请求
 *
 * @author maxwell
 * @since 2021-09-03
 */
public class LyzxRequest {
    /**
     * 设备编号
     */
    private String serial;

    /**
     * 通道编号
     */
    private String code;

    /**
     * 服务器地址
     */
    private String address;

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "LyzxRequest{" +
                "serial='" + serial + '\'' +
                ", code='" + code + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
