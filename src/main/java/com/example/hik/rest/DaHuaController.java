package com.example.hik.rest;

import com.alibaba.fastjson.JSONObject;
import com.dahuatech.hutool.http.Method;
import com.dahuatech.icc.exception.ClientException;
import com.dahuatech.icc.oauth.http.IClient;
import com.dahuatech.icc.oauth.model.v202010.GeneralRequest;
import com.example.hik.rest.dto.HlsUrlRequest;
import com.example.hik.rest.dto.HlsUrlResponse;
import com.example.hik.rest.dto.RtspUrlRequest;
import com.example.hik.rest.dto.RtspUrlResponse;
import com.example.hik.rest.vm.DahuaGeneralRequestVm;
import com.example.hik.rest.vm.DahuaVideoRequestVm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/**
 * @author KJ
 */
@RestController
@RequestMapping("/api")
public class DaHuaController {

    private final static Logger logger = LoggerFactory.getLogger(DaHuaController.class);

    @Autowired
    IClient iClient;

    @PostMapping("/dh")
    public ResponseEntity<String> dahuaRequest(@RequestBody DahuaGeneralRequestVm requestVm) {
        String result;
        try {
            GeneralRequest generalRequest = new GeneralRequest(requestVm.getApiUrl(), Method.valueOf(requestVm.getApiMethod()));
            generalRequest.body(requestVm.getApiParam());
            result = iClient.doAction(generalRequest);
            logger.debug("请求url:{}, 请求参数: {},请求方法: {}, 返回值: {}",
                    requestVm.getApiUrl(),
                    requestVm.getApiParam(),
                    requestVm.getApiMethod(),
                    result);
        } catch (ClientException e) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("err_msg", e.getMessage());
            result = jsonObject.toJSONString();
            logger.error("获取大华接口失败(请求url:{}, 请求参数: {},请求方法: {}): {}",
                    requestVm.getApiUrl(),
                    requestVm.getApiParam(),
                    requestVm.getApiMethod(),
                    e.getMessage(), e);
        }

        return ResponseEntity.ok()
                .header("content-type", "application/json")
                .body(result);
    }

    @PostMapping("/dh/video")
    public ResponseEntity<String> dahuaVideoRequest(@RequestBody DahuaVideoRequestVm videoRequestVm) {
        JSONObject result = new JSONObject();
        try {
            if ("hls".equals(videoRequestVm.getType())) {
                HlsUrlRequest hlsUrlRequest = new HlsUrlRequest(videoRequestVm.getChannelId(), videoRequestVm.getStreamType());
                HlsUrlResponse response = iClient.doAction(hlsUrlRequest, hlsUrlRequest.getResponseClass());
                result.put("desc", response.getDesc());
                result.put("url", response.getHlsUrl());
                result.put("code", response.getCode());
            } else if ("rtsp".equals(videoRequestVm.getType())) {
                RtspUrlRequest rtspUrlRequest = new RtspUrlRequest(videoRequestVm.getChannelId(), videoRequestVm.getStreamType());
                RtspUrlResponse response = iClient.doAction(rtspUrlRequest, rtspUrlRequest.getResponseClass());
                result.put("desc", response.getDesc());
                result.put("url", response.getRtspUrl());
                result.put("code", response.getCode());
                logger.debug("请求参数: {} - {}，返回: {}", videoRequestVm.getChannelId(), videoRequestVm.getStreamType(), response);
            } else {
                result.put("code", "-1");
                result.put("desc", "不支持的取流格式");
                result.put("url", null);
            }
        } catch (ClientException e) {
            result.put("code", "-1");
            result.put("desc", "获取大华视频流失败");
            result.put("url", null);
            logger.error("获取大华视频流失败({} - {} - {}): {}",
                    videoRequestVm.getChannelId(),
                    videoRequestVm.getStreamType(),
                    videoRequestVm.getType(),
                    e.getMessage(), e);
        }

        return ResponseEntity.ok()
                .header("content-type", "application/json")
                .body(result.toJSONString());
    }
}
